module.exports = function() {
    StructureSpawn.prototype.createWorkerCreep =
        function(energy, roleName) {
            var numberOfParts = Math.floor(energy / 200);
            var body = [];
            var newName = roleName + (Math.random()*100).toFixed();
            var totalEnergy = 0;
            for (let i = 0; i < numberOfParts; i++) {
                body.push(WORK);
                totalEnergy += 100;
            }
            for (let i = 0; i < numberOfParts; i++) {
                body.push(CARRY);
                totalEnergy += 50;
            }
            for (let i = 0; i < numberOfParts; i++) {
                body.push(MOVE);
                totalEnergy += 50;
            }

            if (totalEnergy < energy) {
                return this.createCreep(body, newName, { role: roleName});
            } else {
                return null;
            }
        };
};