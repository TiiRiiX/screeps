require('prototype.spawn')();
var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleRepairer = require('role.repairer');
var roleAttacker = require('role.attacker');


module.exports.loop = function () {

    var towers = _.filter(Game.structures, stucture => stucture.structureType == STRUCTURE_TOWER);

    for (let tower of towers){
        var target = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (target){
            tower.attack(target);
        }
    }

    for(let name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    const roles = {
        'harvester' : 4,
        'builder' : 2,
        'upgrader' : 2,
        'repairer' : 2,
        'attacker' : 0
    };

    if (!Game.spawns.Spawn1.spawning){    
        for (let role in roles){
            let groupRole = _.filter(Game.creeps, creep => creep.memory.role == role);
            let newCreep;
            if(groupRole.length < roles[role]) {
                let energy = Game.spawns['Spawn1'].room.energyCapacityAvailable/1.5;
                newCreep = Game.spawns['Spawn1'].createWorkerCreep(energy, role);
            }
            if (newCreep){
                console.log("Spawn new creep: " + newCreep);
            }
        }
    }

    for(let name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if (creep.memory.role == 'repairer') {
            roleRepairer.run(creep);
        }
        if (creep.memory.role == 'attacker') {
            roleAttacker.run(creep);
        }
    }
}