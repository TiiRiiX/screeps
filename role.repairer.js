var roleBuilder = require('role.builder');

var roleHarvester = {
    run: function(creep) {
        if (creep.memory.working && creep.carry.energy == 0) {
            creep.memory.working = false;
            creep.say('harvesting');
            creep.memory.repair = null;
        }
        if (!creep.memory.working && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
            creep.say('repairing');
        }

        if (creep.memory.working) {
            if (!(creep.memory.repair)){
                var structure = creep.room.find(FIND_STRUCTURES, {filter: (s) => s.hits < s.hitsMax});
                structure.sort(function(a, b){if (a.hits > b.hits) return 1; else return -1;})
                creep.memory.repair = structure[0].id;

            }
            if (creep.memory.repair) {
                if (Game.getObjectById(creep.memory.repair).hits < Game.getObjectById(creep.memory.repair).hitsMax) {
                    if (creep.repair(Game.getObjectById(creep.memory.repair)) == ERR_NOT_IN_RANGE)
                        creep.moveTo(Game.getObjectById(creep.memory.repair));
                } else {
                    creep.memory.repair = null;
                }
            }
            else {
                roleBuilder.run(creep);
            }
        }
        else {
            var source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
            if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source);
            }
        }
    }
};

roleHarvester.name = 'repairer';
  
module.exports = roleHarvester;